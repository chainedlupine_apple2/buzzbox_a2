# Buzzbox Music Tracker

This is my Apple II music tracker/sound system.

Download here:  [Buzzbox Tracker v1.0](https://gitlab.com/chainedlupine_apple2/buzzbox_a2/blob/15f205612913b527c1917c5ee79005ee87036c80/releases/buzzbox-v1.0.zip)

(Contains both self-booting 5.25 floppy disk and ProFile image.)

# BUZZBOX.ED

The tracker is in a program called BUZZBOX.ED.  You can invoke it from the ProDOS selector or via BASIC.

# SOUND.S

The actual sound engine exists in sound.s, sound.inc and sound-music.inc.

The music engine takes a series 16-bit values as input.

    Byte 0              Byte 1
    note freq/command   duration/command data

If byte 0<$F0, then this is a note.  Byte 1 will be the duration.  (If duration >128, play at half of current volume)

If byte 1>=$F0, then this is a command.  Subtract $F0 from value to get command number.  Byte 1 will be input to command.

All duration values are from 0 to FF (or 0 to 0x80 for a note).  The value corresponds to the WAIT command in Apple II ROM, or roughly (0.5*(5*(duration\*duration)+27\*duration+26))/10000 seconds.

## Note Frequencies

The note engine cannot meet all standard fundamental frequencies, ergo some notes are impossible.

You can find a list of all known notes (as calibrated by me and my ear -- nothing scientific here!) in sound-music.inc

Octaves 2 through 5 have most of all twelve steps (C, C#, D, D#, E, F, F#, G, G#, A, A#, B), and octaves 6-8 contain only partial steps (Octave 6: C, D, G.  Octave 7: F, G, A.  Octave 8: C, D#, G)

This a limitation of the note engine due to the balanced loops which allow it to generate 7 levels of volume.

## Current Commands

### Command #0: Volume

Second byte contains the volume number, from 1 to 7.

### Command #1: Delay

Second byte contains the amount to delay, from 0 to FF.

## Song Format

The song format is not what sound-music.inc accepts.  It is a meta-format that contains additional information, such as organizing the song in patterns which are then played in sequence (either repeating, or sequentially).

Song data format can be found in buzzbox.s.

A description of the file format (which is the same as what is stored in-memory in the editor) follows.

    Header
    .word   $4254   ; Magic number
    .byte   $00     ; Song length (in sequences), max 128
    .byte   $00     ; Number of used patterns, max 32
    .byte   $00     ; reserved
    .word   $0000   ; reserved
    .word   $0000   ; reserved
    .word   $0000   ; reserved

    Song name
    .res 8      ; 8 byte song name (fixed length string, no zero byte terminator)

    Sequences
    .res 128    ; 128 bytes, initialized as $FF.  $0>= indicates pattern index to use

    Patterns
    .res 8192   ; 8192 bytes of the patterns.  32 patterns, of 256 bytes apiece (128 2-byte sound-music entries)

    Pattern names
    .res 256    ; 256 bytes of pattern names.  32 patterns, each name is 8-bytes (same string format as song name)

## BBT-Converter.py

This python3 program will convert a .BBT song file into a ca65 assembly listing.  

It will then consists of a group of bytes which represents each pattern (with a terminating $00 byte), along with a list of addresses to the patterns in a sequence.  You can then use sound-music::PLAY_SEQ_PTRS to play back the song inside of your own assembly code.

