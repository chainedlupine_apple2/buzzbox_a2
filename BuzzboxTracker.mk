# be careful, any BINFILE name must be 8 chars max
BUILDDST=build/buzzbox

CURR_PATH	:= $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
CURR_MAKEFILE	:= $(lastword $(MAKEFILE_LIST))

BINFILE = BUZZBOX
BINFILE_PATH = $(BUILDDST)
BINFILE_FULL = $(BINFILE_PATH)/$(BINFILE).bin
OBJS = main.o sound.o zeropage.o buzzbox.o fileio.o
LIBS = apple2rom.o prodos.o
TARGET_ARCH = apple2enh
TARGET_MEMORY_MAP = memory-64k.cfg

# disk file to create to hold binfile
DISKIMAGE = $(BUILDDST)/BUZZBOX.PO
DISKVOLUME = BUZZBOX.DISK

HDD_IMAGE_DEST = $(BUILDDST)/BUZZBOX-HDD.2MG
HDD_TEMPLATE = assets/disks/buzzbox-hdd.2mg
DISK_TEMPLATE = assets/disks/buzzbox-floppy.po

ASSETS_DISK =

OBJ_PATH = $(BUILDDST)/obj

PRODOS_BOOT_DISK = $(BUILDDST)/ProDOS_2_4_2.dsk

include lupinecore_a2/binasm.mk
include lupinecore_a2/diskpack.mk
include lupinecore_a2/assettools.mk

SOUND_INC = $(INCLUDE_PATH)/utils.inc src/zeropage.inc src/buzzbox/sound.inc src/buzzbox/sound-music.inc src/buzzbox/buzzbox.inc

COMMON_INCLUDE = $(INCLUDE_PATH)/utils.inc src/zeropage.inc

BUZZ_INC 	= src/buzzbox/buzzbox-pattern-editor.inc src/buzzbox/buzzbox-piano.inc src/buzzbox/buzzbox-ui-texted.inc
BUZZ_INC += src/buzzbox/buzzbox-ui.inc src/buzzbox/buzzbox-note-utils.inc src/buzzbox/buzzbox-settings.inc
BUZZ_INC += src/buzzbox/buzzbox-rawmode.inc src/buzzbox/fileio.inc src/buzzbox/buzzbox-song-editor.inc
BUZZ_INC += src/buzzbox/buzzbox-ui-song.inc src/buzzbox/buzzbox-title.inc src/buzzbox/buzzbox-disk.inc

CPU_TYPE = 6502

$(BUILDDST)/obj/zeropage.o:	src/zeropage.s src/zeropage.inc
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/fileio.o: src/buzzbox/fileio.s src/buzzbox/fileio.inc $(COMMON_INCLUDE)  $(INCLUDE_PATH)/prodos.inc $(INCLUDE_PATH)/prodos-defs.inc $(INCLUDE_PATH)/prodos-snippets.inc
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/sound.o: src/buzzbox/sound.s $(SOUND_INC)
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/buzzbox.o: src/buzzbox/buzzbox.s $(SOUND_INC) $(BUZZ_INC)
	$(CA65) $(AFLAGS) $< -o $@

$(BUILDDST)/obj/main.o: src/buzzbox/main.s src/buzzbox/fileio.inc $(SOUND_INC)
	$(CA65) $(AFLAGS) $< -o $@

# kinda ugly, recursive makes but didn't want to use .phonys
$(DISKIMAGE): $(BINFILE_FULL) $(ASSETS_DISK1)
	-cp -f $(DISK_TEMPLATE) $(DISKIMAGE)
	$(AC) -p $(DISKIMAGE) BUZZBOX.ED sys 0x2000 <$(BUILDDST)/BUZZBOX.bin
	$(AC) -k $(DISKIMAGE) BUZZBOX.ED
	$(AC) -p $(DISKIMAGE) SONGS/FLYME.BBT bin 0x0000 <assets/examples/flyme1.bbt
	$(AC) -k $(DISKIMAGE) SONGS/FLYME.BBT
	$(AC) -p $(DISKIMAGE) STARTUP bas 0x0801 <assets/STARTUP#fc0801

$(HDD_IMAGE_DEST): $(BINFILE_FULL) $(ASSETS_DISK1) $(HDD_TEMPLATE)
	-cp -f $(HDD_TEMPLATE) $(HDD_IMAGE_DEST)
	$(AC) -d $(HDD_IMAGE_DEST) BUZZBOX/BUZZBOX.ED
	$(AC) -p $(HDD_IMAGE_DEST) BUZZBOX/BUZZBOX.ED sys 0x2000 <$(BUILDDST)/BUZZBOX.bin
	$(AC) -k $(HDD_IMAGE_DEST) BUZZBOX/BUZZBOX.ED
	$(AC) -p $(HDD_IMAGE_DEST) BUZZBOX/SONGS/FLYME.BBT bin 0x0000 <assets/examples/flyme1.bbt
	$(AC) -k $(HDD_IMAGE_DEST) BUZZBOX/SONGS/FLYME.BBT
	$(AC) -p $(HDD_IMAGE_DEST) STARTUP bas 0x0801 <assets/STARTUPHDD#fc0801



mount: $(DISKIMAGE)
	-cp -f $(DISKIMAGE) ../../ADTPro-2.0.3/disks/BUZZBOX/Virtual.po

test: $(DISKIMAGE)
	-lupinecore_a2/extern/AppleWin/Applewin.exe -no-printscreen-key -d1 $(CURR_PATH)/$(DISKIMAGE) -s7 empty

testhdd: $(HDD_IMAGE_DEST)
	-lupinecore_a2/extern/AppleWin/Applewin.exe -no-printscreen-key -s7 $(HDD_IMAGE_DEST)

hdd: $(HDD_IMAGE_DEST)

.PHONY: all clean cleanassets assets hdd cleandsk dsk test cleantxt mount disks

assets: $(ASSETS_DISK)

all: | bin dsk

dsk: $(DISKIMAGE)

disks: dsk hdd

cleanassets: $(ASSETS)
	-rm -f $(ASSETS_DISK)

cleandsk:
	-rm -f $(DISKIMAGE)
	-rm -f $(HDD_IMAGE_DEST)

clean: cleanbin cleandsk
