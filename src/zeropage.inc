; -- zeropage definitions --------------------
; note: TOTALLY stomps all over Applesoft/Integer BASIC, do not use together!
.feature c_comments

.segment "ZEROPAGE"

.global arg1, arg2, arg3, arg3, arg4, arg5, arg6
.global arg1w, arg2w, arg3w, arg4w, arg5w
.global temp1, temp2, temp3, temp4, temp5, temp6
.global temp1w, temp2w, temp3w, temp4w, temp5w

.segment "CODE_END_H"

.global _ZP_RESTORE, _ZP_SAVE, _ZP_SETUP

.ifndef ZP_INC_GUARD
ZP_INC_GUARD := 1



    ; switch back to high CODE space, just in case I forgot to add this to my .s file
  ;.segment "CODE_H"
    
.endif

.macro ZP_SETUP
  jsr _ZP_SETUP
.endmacro

; save from arg1 ($60) to temp5w ($7F)
.macro ZP_SAVE
  jsr _ZP_SAVE
.endmacro

.macro ZP_RESTORE
  jsr _ZP_RESTORE
.endmacro


.segment "CODE_H"