; Converts ProDOS stub routines into actual fileio calls

.include "apple2rom.inc"
.include "apple2rom-defs.inc"
.include "prodos.inc"
.include "utils.inc"
.include "fileio.inc"
.include "../zeropage.inc"
.include "prodos-snippets.inc"

.segment "CODE_END_H"

;DEBUG_PRODOS := 1

.feature addrsize

    ; arg1w = addr to filename path (as LPSTR)
    ; return = no carry if successful, otherwise error in accum
SUB FILE_OPEN
    ; ------- attempt to load a file into some buffers -----
    Snippet_OPEN_FILE arg1w, fileio_open_param
    rts
END_SUB

; copy the open file REF_NUM over to all other structs, so they will work
SUB FILE_PREPARE
    Snippet_COPY_PARAM_REFNUM fileio_open_param, fileio_read_param, fileio_write_param, fileio_close_param
    rts
END_SUB

; arg1w = addr to filename (LPSTR)
; return: carry if error, error in accum
SUB FILE_CREATE
    Snippet_FILE_CREATE arg1w, fileio_create_param
    rts
END_SUB

SUB FILE_CLOSE
    ProDOS_CLOSE fileio_close_param
    rts
END_SUB

; arg1w = buffer to write
; arg2w = size of data to write
SUB FILE_WRITE
    Snippet_FILE_WRITE arg1w, arg2w, fileio_write_param
    rts
END_SUB

; arg1w = buffer to read into
; arg2w = size of data to read
SUB FILE_READ
    Snippet_FILE_READ arg1w, arg2w, fileio_read_param
    rts

END_SUB

; A = ProDOS error code
SUB EXIT_PRODOSERROR
    PHA
    JSR ROM::TEXT_MODE_40
    ROM_PRINT #error_file_msg
    PLA
    JSR ROM::ROM_PRBYTE
    JSR ROM::ROM_GETKEY ; GETKEY

    JMP QUIT_TO_PRODOS
END_SUB

; arg1w = path to delete
SUB FILE_DESTROY
    Snippet_FILE_DESTROY arg1w, fileio_destroy_param
    rts
END_SUB

; arg1w = prefix to set (this also sets boot_volname)
SUB FILE_SET_PREFIX_FROM_LASTPATHNAME
    Snippet_SET_PREFIX #ProDOS::LAST_PATHNAME, fileio_getset_prefix_param
    rts
END_SUB

; arg1w = prefix to set (this also sets boot_volname)
SUB FILE_SET_PREFIX
    Snippet_SET_PREFIX arg1w, fileio_getset_prefix_param
    rts
END_SUB

SUB FILE_GET_PREFIX_INTO_CURRPATH
    Snippet_GET_PREFIX #ProDOS::LAST_PATHNAME, fileio_getset_prefix_param

    ; check for null prefix, if so, then we need to get our volume name
    lda   ProDOS::LAST_PATHNAME
    bne   no_volname_necessary
    Snippet_GET_VOLUME_NAME fileio_curr_unitnum, fileio_on_line_param
    bcs   :+
  no_volname_necessary:
    jsr   FILE_SET_LASTPATHNAME_TO_NEW_PREFIX
    clc
  :
    rts
END_SUB


SUB FILE_GET_CURR_UNITNUM
    jsr FILE_GET_CURR_PATH_INFO
    rts
END_SUB

SUB FILE_GET_CURR_UNITNUM_AND_PATH
    jsr   FILE_GET_CURR_PATH_INFO

    jsr   FILE_SET_LASTPATHNAME_TO_NEW_PREFIX

    rts
END_SUB

; looks through all mounted volumes and figures out where we are mounted from
SUB FILE_RESCAN_CURR_UNITNUM_FROM_PATH
    Snippet_FIND_VOLUME_NAME fileio_buffer, fileio_curr_path, fileio_curr_unitnum, fileio_on_line_param
    rts
END_SUB


SUB FILE_RESCAN_CURR_UNITNUM_FROM_LAST_ACCESS
    Snippet_GET_CURR_UNITNUM fileio_curr_unitnum
    rts
END_SUB

SUB FILE_SET_LASTPATHNAME_TO_NEW_PREFIX
    jsr   FILE_SET_PREFIX_FROM_LASTPATHNAME
    bcs   :+
    Util_LOAD #ProDOS::LAST_PATHNAME, arg1w
    ; if all good, copy LAST_PATHNAME to fileio_curr_path
    jsr   FILE_LSTR_TO_CURR_PATH
  :
    rts
END_SUB

; writes the volname into $280 (LASTPATH) so if we want to save it we need to copy it over to somewhere else
SUB FILE_GET_CURR_PATH_INFO
    Snippet_GET_PATH_INFO fileio_curr_unitnum, fileio_getset_prefix_param, fileio_on_line_param
    rts
END_SUB

    ; forces all devices to rescan their ProDOS volumes
SUB FILE_DISK_ALL_ON_LINE
    ; we don't care about results, just use io_buffer
    Snippet_DISK_ALL_ON_LINE fileio_buffer, fileio_on_line_param
    rts
END_SUB

    ; checks the volume name found in the curr_unitnum drive (usually slot 6, drive 1)
    ; if it differs from our current logged pathname, we need to rescan.  (A disk was changed.)
    ; returns:
    ;   carry will be set if we had to reload the volume name
    ;   carry clear = no new volume, but check a>0 to see if ProDOS error
SUB FILE_RESYNC_VOLUME_NAME
    Snippet_RESYNC_VOLUME_NAME fileio_curr_path, fileio_curr_unitnum, fileio_on_line_param, fileio_getset_prefix_param
    rts
END_SUB

; arg1w = source to copy (LSTR)
SUB FILE_LSTR_TO_CURR_PATH
    Snippet_LSTR_TO_PATH arg1w, fileio_curr_path
    rts
END_SUB

dir_load_size := $0200

; arg1w = buffer to use for directory file
; arg2w = routine to call with the file entry data
SUB FILE_SHOW_CATALOG
    Snippet_GET_CATALOG arg1w, arg2w, #fileio_curr_path, dir_load_size, fileio_open_param, fileio_read_param, fileio_close_param
    rts
END_SUB

.segment "RODATA_H"

DEFDATA error_file_msg
  .asciiz "Fatal ProDOS error: code = "
END_DEFDATA

.segment "DATA_H"

DEFDATA fileio_curr_unitnum
  .byte $00
END_DEFDATA

DEFDATA	fileio_curr_path
  .res $40, $00
END_DEFDATA

DEFDATA fileio_open_param
  ProDOS_DEFINE_OPEN_PARAM fileio_buffer
END_DEFDATA

DEFDATA fileio_quit_param
  ProDOS_DEFINE_QUIT_PARAM
END_DEFDATA

DEFDATA fileio_read_param
  ProDOS_DEFINE_READ_PARAM
END_DEFDATA

DEFDATA fileio_write_param
  ProDOS_DEFINE_WRITE_PARAM
END_DEFDATA

DEFDATA fileio_close_param
  ProDOS_DEFINE_CLOSE_PARAM
END_DEFDATA

DEFDATA fileio_create_param
  ProDOS_DEFINE_CREATE_PARAM
END_DEFDATA

DEFDATA fileio_on_line_param
  ProDOS_DEFINE_ON_LINE_PARAM
END_DEFDATA

DEFDATA fileio_getset_prefix_param
  ProDOS_DEFINE_GETSET_PREFIX_PARAM
END_DEFDATA

DEFDATA fileio_destroy_param
  ProDOS_DEFINE_DESTROY_PARAM
END_DEFDATA

.segment "DATA_L"

DEFDATA fileio_buffer 	; 1024 bytes
  .res $400, $00
END_DEFDATA
