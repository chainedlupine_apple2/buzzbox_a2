.include "../zeropage.inc"
.include "utils.inc"
.include "sound.inc"
.include "apple2rom.inc"

.include "sound.inc"
.include "sound-music.inc"
.include "fileio.inc"

.include "buzzbox-note-utils.inc"
.include "buzzbox-ui.inc"
.include "buzzbox-ui-texted.inc"
.include "buzzbox-pattern-editor.inc"
.include "buzzbox-piano.inc"
.include "buzzbox-settings.inc"
.include "buzzbox-rawmode.inc"
.include "buzzbox-song-editor.inc"
.include "buzzbox-ui-song.inc"
.include "buzzbox-title.inc"
.include "buzzbox-disk.inc"

.include "buzzbox.inc"

.segment "DATA_H"

temp:
  .res 8

; --------- song data --------------
song_data_begin_marker:
; 12-byte header
songheader:
  .word $0000   ; BT ($42, $54)
  .byte $00     ; song len (in patterns)
  .byte $00     ; number of patterns
  .byte $00     ; version 0=initial
  .byte $00     ; unused
  .word $0000   ; unused
  .word $0000   ; unused
  .word $0000   ; unused
songheader_end:

songname:
  .res 8

; just 128 slots, a byte apiece (that indexes a pattern)
seqstorage:
  .res 128

; storage for 32 patterns, at 128 commands each (256 bytes per, total of 8192 bytes)
patternstorage:
  .res max_patterns * 2 * song_len

; 32 patterns, 8 characters apiece
patternnames:
  .res max_patterns * 8

song_data_end_marker:
; --------- song data --------------
