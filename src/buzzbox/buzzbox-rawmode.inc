.segment "CODE_H"

SUB STARTUP_RAWMODE
    jsr CLEAR_UI_PATTERN_KEYS
    jsr DRAW_UI_RAWMODE
    jsr DRAW_UI_RAWMODE_EXTRA
    rts
END_SUB

SUB SHUTDOWN_RAWMODE
    jsr CLEAR_UI_PATTERN_KEYS
    jsr DRAW_UI_PATTERN_KEYS
    rts
END_SUB

SUB DO_RAWMODE
    inkey   = temp1
    note    = temp2

  keywait:
	:	lda ROM::IN_KEYB
		bpl :-

    and #$7F
    sta inkey

		sta ROM::SB_KEYB

    lda inkey
    cmp #ROM::KB::kb_escape
    bne :+
      sec
      rts
    :

    cmp #ROM::KB::kb_enter
    bne :+
      clc
      rts
    :

    cmp #ROM::KB::kb_left_arrow
    bne :+
      jsr MOVE_PATTERN_CURSOR_LEFT
      lda pattern_cursor
      sta ui_piano_original_cursor
      jmp keywait
  :

    cmp #ROM::KB::kb_right_arrow
    bne :+
      jsr MOVE_PATTERN_CURSOR_RIGHT
      lda pattern_cursor
      sta ui_piano_original_cursor
      jmp keywait
  :

    cmp #ROM::KB::kb_K
    bne :+
      lda pattern_cursor
      jsr DELETE_PATTERN_NOTE
      jsr DRAW_PATTERN
      jsr PLAY_THUNK
      jmp keywait
  :

    cmp #ROM::KB::kb_N
    bne :+
      jsr CLEAR_PATTERN_CURSOR

      ; restore cursor position
      lda ui_piano_original_cursor
      sta pattern_cursor
      
      ; restore pattern
      jsr RESTORE_PATTERN

      jsr CALC_PATTERN_CURSOR
      jsr DRAW_PATTERN_CURSOR
      jsr DRAW_PATTERN_EXTRA
      jsr DRAW_PATTERN

      jsr PLAY_LONG
      jmp keywait
  :

    cmp #ROM::KB::kb_1
    bne :+
      lda #du_2
      sta rawmode_dur
      jsr DRAW_UI_RAWMODE_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_2
    bne :+
      lda #du_3
      sta rawmode_dur
      jsr DRAW_UI_RAWMODE_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_3
    bne :+
      lda #du_4
      sta rawmode_dur
      jsr DRAW_UI_RAWMODE_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_4
    bne :+
      lda #du_5
      sta rawmode_dur
      jsr DRAW_UI_RAWMODE_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_5
    bne :+
      lda #du_7
      sta rawmode_dur
      jsr DRAW_UI_RAWMODE_EXTRA
      jmp keywait
    :

    cmp #ROM::KB::kb_P
    bne :+
      jsr CLEAR_UI_PATTERN_KEYS
      jsr PLAY_PATTERN_WITH_CALLBACK
      jsr DRAW_UI_RAWMODE
      jsr DRAW_UI_RAWMODE_EXTRA
      jmp keywait
  :

    cmp #ROM::KB::kb_LEFTBRACKET  ; duration less
    bne :+++
      lda rawmode_dur
      sec
      sbc #1
      ; if no carry, then we wrapped around
      bcs :+
        lda #1
      :
      bne :+
        lda #1
      :
      sta rawmode_dur
      jsr DRAW_UI_RAWMODE_EXTRA
      jmp keywait
  :

    cmp #ROM::KB::kb_RIGHTBRACKET  ; duration more
    bne :++
      lda rawmode_dur
      clc
      adc #1
      cmp #$80
      ; if carry, then we are greater-than-equal
      bcc :+
        lda #$7F
      :
      sta rawmode_dur
      jsr DRAW_UI_RAWMODE_EXTRA
      jmp keywait
  :
    cmp #ROM::KB::kb_up_arrow
    bne :+
      inc rawmode_freq
      jsr DRAW_UI_RAWMODE_EXTRA
      jsr RAWMODE_PLAYNOTE
      jmp keywait
    :

    cmp #ROM::KB::kb_down_arrow
    bne :+
      dec rawmode_freq
      jsr DRAW_UI_RAWMODE_EXTRA
      jsr RAWMODE_PLAYNOTE
      jmp keywait
    :

    cmp #ROM::KB::kb_plus  ; vol up
    bne :++
      inc rawmode_vol      
      lda rawmode_vol
      cmp #7
      bcc :+
        lda #7
        sta rawmode_vol
      :
      jsr DRAW_UI_RAWMODE_EXTRA
      jsr RAWMODE_PLAYNOTE
      jmp keywait
  :
    
    cmp #ROM::KB::kb_minus  ; vol down
    bne :++
      dec rawmode_vol
      bne :+
        lda #1
        sta rawmode_vol
      :
      jsr DRAW_UI_RAWMODE_EXTRA
      jsr RAWMODE_PLAYNOTE
      jmp keywait
  :

    jmp keywait
END_SUB

SUB RAWMODE_PLAYNOTE
    lda rawmode_freq
    sta sound_freq
    lda rawmode_dur
    cmp #25
    bcc :+
      lda #25
    :
    sta sound_duration
    lda rawmode_vol
    sta sound_volume
    jsr PLAY_FREQ
    rts
END_SUB

SUB DRAW_UI_RAWMODE
    lda #0
    ldx #13
    jsr SET_CURSOR

    ROM_PRINT_CINV #txt_rawmode_help

    lda #0
    ldx #15
    jsr SET_CURSOR

    ROM_PRINT_CINV #txt_rawmode_help2

    rts
END_SUB

SUB DRAW_UI_RAWMODE_EXTRA
    lda #0
    ldx #18
    jsr SET_CURSOR

    ROM_PRINT #txt_rawmode_output_freq

    lda rawmode_freq
    jsr ROM::ROM_PRBYTE

    ROM_PRINT #txt_rawmode_output_dur

    lda rawmode_dur
    jsr ROM::ROM_PRBYTE

    ROM_PRINT #txt_rawmode_output_vol

    lda rawmode_vol
    jsr ROM::ROM_PRBYTE


    rts
END_SUB

; insert modes using the old raw system
SUB INSERT_RAW_MODE
    lastvol = temp
    lastdur = temp+1

    jsr PATTERN_FIND_LAST_CURSOR_INFO
    sta lastvol ; last volume
    sta rawmode_vol
    stx lastdur ; last duration
    stx rawmode_dur

    ; copy pattern
    jsr BACKUP_PATTERN

    jsr STARTUP_RAWMODE

  loop:
    jsr PATTERN_FIND_LAST_CURSOR_INFO
    sta lastvol ; last volume
    stx lastdur ; last duration

    jsr DO_RAWMODE
    bcc :+
      jmp exit
    :

    jsr CLEAR_PATTERN_CURSOR

    ; use rawmode_xxx to fill in note
    jsr INSERT_RAWMODE_NOTE

    lda pattern_cursor
    sta ui_piano_original_cursor

    jsr CALC_PATTERN_CURSOR
    jsr DRAW_PATTERN_CURSOR
    jsr DRAW_PATTERN_EXTRA

    jsr DRAW_PATTERN

    jmp loop

  exit:
    jsr SHUTDOWN_RAWMODE
    rts

END_SUB

SUB INSERT_RAWMODE_NOTE
    lastvol = temp

    lda pattern_cursor
    cmp #$80
    bcc :+
      jsr PLAY_ERROR
      rts
    :

    lda rawmode_vol
    cmp lastvol
    beq :+
      ; volume change, must add new command
      lda pattern_cursor
      jsr INSERT_BLANK_PATTERN_NOTE

      lda #cmd_v
      ldx rawmode_vol
      jsr MODIFY_CURR_PATTERN_NOTE

      jsr INC_PATTERN_CURSOR

      lda rawmode_vol
      sta lastvol
    :

    ; now add the note
    lda pattern_cursor
    jsr INSERT_BLANK_PATTERN_NOTE

    lda rawmode_freq
    ldx rawmode_dur
    jsr MODIFY_CURR_PATTERN_NOTE

  exit:
    jsr INC_PATTERN_CURSOR

    lda rawmode_freq
    sta sound_freq
    lda rawmode_dur
    sta sound_duration
    lda rawmode_vol
    sta sound_volume
    jsr PLAY_FREQ

    rts
END_SUB


.segment "RODATA_H"

DEFDATA txt_rawmode_help
  INVBYTE 'E'
  INVBYTE 'S'
  INVBYTE 'C'
  .byte "=EXIT "
  INVBYTE '<'
  .byte "/"
  INVBYTE '>'
  .byte "=CURSOR "
  INVBYTE 'K'
  .byte "ILL U"
  INVBYTE 'N'
  .byte "DO "
  INVBYTE 'R'
  INVBYTE 'E'
  INVBYTE 'T'
  .byte "=ADD "
  .byte 0
END_DEFDATA

DEFDATA txt_rawmode_help2
  INVBYTE '1'
  .byte "-"
  INVBYTE '5'
  .byte "=DUR# "
  INVBYTE 'U'
  .byte "/"
  INVBYTE 'D'
  .byte "=FREQ "
  INVBYTE '['
  .byte "/"
  INVBYTE ']'
  .byte "=DUR "
  INVBYTE '-'
  .byte "/"
  INVBYTE '+'
  .byte "=VOL "
  INVBYTE 'P'
  .byte "=PLAY"
  .byte 0
END_DEFDATA

DEFDATA txt_rawmode_output_freq
  .asciiz "FREQ="
END_DEFDATA

DEFDATA txt_rawmode_output_vol
  .asciiz " VOL="
END_DEFDATA

DEFDATA txt_rawmode_output_dur
  .asciiz " DUR="
END_DEFDATA

.segment "DATA_H"

DEFDATA rawmode_freq
  .byte $20
END_DEFDATA

DEFDATA rawmode_vol
  .byte $5
END_DEFDATA

DEFDATA rawmode_dur
  .byte du_3
END_DEFDATA