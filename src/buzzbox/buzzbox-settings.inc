.segment "CODE_H"

SUB DO_SETTINGS_EDITOR
    inkey = temp1

    JSR ROM::ROM_HOME ; home, clrscreen

    jsr DRAW_UI_TOP

    jsr DRAW_SETTINGS_UI

		sta ROM::SB_KEYB

  loop:
	:	lda ROM::IN_KEYB
		bpl :-

    and #$7F
    sta inkey

		sta ROM::SB_KEYB

    lda inkey
    cmp #ROM::KB::kb_escape
    bne :+
      rts
    :

    cmp #ROM::KB::kb_I
    bne :++
      inc settings_piano
      lda settings_piano
      cmp #2
      bne :+
      lda #0
      :
      sta settings_piano      
      jsr DRAW_SETTINGS_UI
    :

    jmp loop
END_SUB


SUB DRAW_SETTINGS_UI
    lda #0
    ldx #3
    jsr SET_CURSOR

    ROM_PRINT #txt_settings_help

    lda #2
    ldx #5
    jsr SET_CURSOR

    ROM_PRINT_CINV #txt_settings_piano

    lda settings_piano
    jsr ROM::ROM_HEX
    rts
END_SUB

SUB CLEAR_SETTINGS_UI
    ldy #7
  loop:
    tya
    clc
    adc #3
    tax
    lda #1
    U_PHY
    jsr SET_CURSOR
    jsr ROM::ROM_CLREOL
    U_PLY
    dey
    bne loop
    rts
END_SUB

.segment "RODATA_H"

DEFDATA txt_settings_help
    .asciiz "Tracker Settings (ESC=RETURN)"
END_DEFDATA

DEFDATA txt_settings_piano
  .byte "P"
  INVBYTE 'I'
  .byte "ANO ENABLE="
  .byte 0
END_DEFDATA

.segment "DATA_H"

DEFDATA settings_piano  ; piano mode on and off
  .byte 1
END_DEFDATA

