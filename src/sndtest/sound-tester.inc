.segment "CODE_H"

SOUND_TESTER:
    jsr ROM_HOME

    lda #10
    sta sound_duration

  @ui_loop:
    jsr PRINT_PROMPT

		sta $c010 ; clear strobe

	@keyloop:
    lda continuous
    beq :+
      lda #40
      sta sound_duration
      jsr PLAY_FREQ  
    :  

    lda $c000
		bpl @keyloop

		and #$7f
		sta temp1
		sta $c010

		; key press in temp1
		lda temp1
    cmp #ROM::KB::kb_up_arrow
		bne :+
		jsr @key_up
	:
    cmp #ROM::KB::kb_down_arrow
		bne :+
		jsr @key_down
	:
    cmp #ROM::KB::kb_left_arrow
		bne :+
		jsr @key_left
	:
    cmp #ROM::KB::kb_right_arrow
		bne :+
		jsr @key_right
	:
    cmp #ROM::KB::kb_1
    bne :+
    jsr @key_1
  :
    cmp #ROM::KB::kb_2
    bne :+
    jsr @key_2
  :
    cmp #ROM::KB::kb_3
    bne :+
    jsr @key_3
  :
    cmp #ROM::KB::kb_4
    bne :+
    jsr @key_4
  :
    cmp #ROM::KB::kb_5
    bne :+
    jsr @key_5
  :

    cmp #ROM::KB::kb_8
    bne :+
    jsr @key_8
  :

    cmp #ROM::KB::kb_9
    bne :+
    jsr @key_9
  :

    jmp @ui_loop

  @reset_continuous:
    rts

  @key_up:
    inc sound_freq
    jsr @reset_continuous
    rts

  @key_down:
    dec sound_freq
    jsr @reset_continuous
    rts

  @key_left:
    dec sound_volume
    bne :+
      lda #1
      sta sound_volume
    :
    rts

  @key_right:
    inc sound_volume
    lda sound_volume
    cmp #8
    bcc :+
      lda #7
      sta sound_volume
    :
    rts

  @key_1:
    lda #$20
    jmp @playit

  @playit:
    sta sound_duration
    jsr PLAY_FREQ
    lda #0
    sta continuous
    rts

  @key_2:
    lda #$35
    jmp @playit

  @key_3:
    lda #$50
    jmp @playit

  @key_4:
    lda #$70
    jmp @playit

  @key_5:
    lda #$ff
    jmp @playit

  @key_8:
    lda #0
    sta continuous
    rts

  @key_9:
    lda #1
    sta continuous
    rts

PRINT_PROMPT:
    ROM_PRINT #prompt
    lda sound_freq
    JSR ROM_PRBYTE

    ldx #1
    JSR ROM_PRBL2

    ROM_PRINT #prompt2

    lda sound_volume
    JSR ROM_PRBYTE

    JSR ROM_CROUT

    rts



.segment "DATA_H"

continuous:
  .byte 0

.segment "RODATA_H"


prompt:
  .asciiz "Freq="

promptspc:
  .asciiz " "

prompt2:
  .asciiz "Vol="

